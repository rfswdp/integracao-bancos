############################################
# created by: Raphael Schubert
# classe que integra servicos do itau
# extrato de depositos identificados
####
class Itau():

    def __init__(self):
        self.__REGISTROS = []

    def processa_arquivo_extrato_depositos_identificados(self, arquivo):
        with open(arquivo) as f:
            lines = f.readlines()

        for linha in lines:
            self.__REGISTROS.append(self.processa_linha(linha))

        return self

    def resultado(self):
        return self.__REGISTROS

    def processa_linha(self, linha):
        # processa a linha do extrato
        __exploded_array = linha.split(";")
        return {
            "numero_doc":__exploded_array[1],
            "nome_remetente":__exploded_array[2],
            "documento":self.helper_format_documento(__exploded_array[3]),
            "data":__exploded_array[4],
            "valor":float(__exploded_array[5])/100,
            "tipo":__exploded_array[6],
            "banco":__exploded_array[7].split("/")[0],
            "ispb":__exploded_array[7].split("/")[1],
            "agencia":__exploded_array[8],
            "conta":__exploded_array[9],
            "descricao":__exploded_array[12],
            "extra_data":[__exploded_array[0], __exploded_array[10], __exploded_array[11]]
        }

    def helper_format_documento(self, documento):
        # formata CPF/CNPJ
        if documento[:3] == '000':
            return {
                "tipo":"cpf",
                "numero":"{}.{}.{}-{}".format(documento[3:6],documento[6:9],documento[9:12],documento[12:15])
            }
        else:
            # todo: incluir formatacao para CNPJ
            return {
                "tipo":"cnpj",
                "numero":documento
            }
