# Classes de Integração com bancos

## Objetivo

O objetivo deste repositório, é concentrar soluções práticas de classes em Python para integrar serviços bancários. Desde leitura de extratos até emissão de boletos ou outros serviços que sejam possíveis serem executados por automação.

### Itaú

No momento a classe do Itaú está sendo melhorada, e em breve conterá mais funcionalidades. No momento ela apenas lê o extrato de depósito identificado de uma conta.

É necessário entrar no internet banking e fazer o download do extrato no formato .txt e em seguida, passar para a classe efetuar a leitura e retornar os dados.

O formato de retorno da classe do Itaú será:

```python
{
	'numero_doc': '0000045096',
	'nome_remetente': 'ELAINE CRISPINN FERREIRA PEREI',
	'documento': {
		'tipo': 'cpf',
		'numero': '000.000.000-00'
	},
	'data': '23/07/2018',
	'valor': 300.0,
	'tipo': 'TED  ',
	'banco': '033',
	'ispb': '90400888',
	'agencia': '3541',
	'conta': '00000109000',
	'descricao': 'CREDITO EM CONTA CORRENTE\n',
	'extra_data': ['8262405353', '38', '01']
}
```

O campo `extra_data` contém informações que não estavam disponíveis para mim no momento da leitura da documentação do Itaú, portanto, embora não saiba exatamente para que serve este dado, ele é armazenado em um array para uso/consulta em caso de necessidade.
